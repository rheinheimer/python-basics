""" Calculadora de média. """

b1 = float(input('Insira a nota do primeiro bimestre: '))
b2 = float(input('Insira a nota do segunda bimestre: '))
b3 = float(input('Insira a nota do terceiro bimestre: '))
b4 = float(input('Insira a nota do quarto bimestre: '))

media = (b1 + b2 + b3 + b4)/4

print('A média final é {}'.format(media))
