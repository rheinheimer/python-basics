""" Calculadora de média condicional. """

p1 = float(input('Insira a nota da primeira prova: '))
p2 = float(input('Insira a nota da segunda prova: '))
p3 = float(input('Insira a nota da terceira prova: '))
p4 = float(input('Insira a nota da quarta prova: '))

media = (p1 + p2 + p3 + p4)/4

if media >= 5:
    print('Sua média foi {}'.format(media))
    print('Você passou de ano')
else:
    print('Sua média foi {}'.format(media))
    print('Você reprovou')
