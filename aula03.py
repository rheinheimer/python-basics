""" Aula 03. """

# Estruturas de repetição -> For e While
# Eu quero escrever uma sequência de 0 até 10
# print('0, 1, 2, ...')

# for -> para -> repetição que eu sei o limite, ou o final
# while -> enquanto -> repetição que eu não sei quando acaba

# for:

# para variável dentro de intervalo
for n in range(0, 11):
    print(n)
# loop

# Operadores Booleanos
# True -> Verdade
# False -> Falso

# Iniciando o contador
cont = 0

# Enquanto verdadeiro
while True:
    print(cont)

    cont = cont + 1

    if cont == 11:
        break

# break = Pare!
