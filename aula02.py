# Estruturas condicionais - If, Elif e Else

# If - Se
# Elif - Senão
# Else - Então

# SE EU COMER BANANA:
#   EU VOU SER SAUDÁVEL
# SE EU NÃO COMER BANANA:
#   EU NÃO VOU SER SAUDÁVEL

nome = input('Insira seu nome: ')

# = -> Recebe
# == -> Igual

if nome == 'Amanda':
    print('Amor da minha vida')
elif nome == 'João':
    print('Esse é meu nome')
else:
    print('Não é o amor da minha vida nem meu nome')

print('Fim do programa')

# Estrutura de repetição - While e For

# While - Enquanto
# For - Para
