""" Aula 04. """

# Importações

# importar bibliotecas
# bibliotecas -> conjunto de comandos que permite a execução
#  de problemas específicos


# exemplo
# angulo -> Graus ou Radianos

# 180 - pi radianos
# x graus - pi/2 radianos
# x radianos = pi*90/180

# Com importação
# importar nome da biblioteca
import math
import os

print(math.radians(180))
