# Tipos de variáveis - Tipos primitivos

# variável --> um espaço da memória onde iremos guardar uma informação
# tipos de variáveis:
# inteiros -->
# floats -->
# strings -->

a = 'maçã'  # string
b = 'banana'  # string

c = 2  # int
d = 4  # int

e = 3.5  # float
f = 6.9  # float

# Comandos de interação com o usuário

# print --> Escreve algo na tela
# input --> Agrego um valor pra uma variável

print('Hello World')

nome = input('Insira o seu nome: ')

print('Seu nome é', nome)

# Operadores

# + --> Soma
# - --> Subtração
# * --> Multiplicação
# / --> Divisão
# ** --> Potenciação
# != --> Diferença
# % --> Resto da divisão

# Strings
print(a + b)
t = a + b
print(t)

# Inteiros
print(c*d)
u = c*d
print(u)
