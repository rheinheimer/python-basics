# Aula 05
# Listas, tuplas e dicionários em Python

# Listas, tuplas e dicionários são variáveis compostas dentro do python

# Listas
# Conjunto de variáveis que pode sofrer mutação 
# (Isto é, podem ser adicionadas, subtraídas, multiplicadas, etc)
# Sintaxe:

a1 = [3, 5, 'banana', True, 7.9]
print(a1)

# ou

a2 = list(3, 5, 'banana', True, 7.9)
print(a2)

# Mutação

print(a1 + a2)
print(a1 - a2)
print(a1*a2)

# Tuplas
# Conjunto de variáveis que não pode sofrer mutação
# (Suas variáveis são imutáveis, não aceitam mudanças)
# Sintaxe:

b1 = (9, 3.2, 'maça', False)
print(b1)

# ou

b2 = tuple(9, 3.2, 'maça',)


# Dicionários
